"""Unit tests"""
from blockchain.blockchain import Blockchain
from blockchain.block import Block


def test_blockchain():
    """Test blockchain verification"""
    # initialize blockchain
    block_chain = Blockchain()

    # mine blocks
    for i in range(4):
        block_chain.mine(Block("data packet " + str(i + 1)))

    block_chain.show_blocks()

    assert block_chain.verify() is True

    # Modify third (index: 2) block
    block_chain.head.next_block.next_block.data = "Dirty data"

    assert block_chain.verify() is False
