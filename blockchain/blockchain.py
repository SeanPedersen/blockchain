"""Blockchain"""
from typing import Optional
from blockchain.block import Block


class Blockchain:
    """Blockchain"""

    mining_difficulty = 20
    max_nonce = 2 ** 32
    hash_target_size = 2 ** (256 - mining_difficulty)

    def __init__(self):
        # Init first (genesis) block in the blockchain
        self.last: Optional[Block] = None
        self.head: Block = Block("Genesis")
        self.mine(self.head)

    def add(self, block: Block):
        """Add block to blockchain"""
        # Update last block
        if self.last:
            self.last.next_block = block
        self.last = block

    def mine(self, block: Block):
        """Search correct block's hash nonce """
        if self.last:
            block.previous_hash = self.last.hash()
            block.block_index = self.last.block_index + 1

        for _ in range(self.max_nonce):
            if int(block.hash(), 16) <= self.hash_target_size:
                self.add(block)
                print(block.block_index, block.nonce)
                break
            else:
                block.nonce += 1

    def verify(self) -> bool:
        """ Verify integrity of all blocks"""
        previous_hash = 0x0
        current_block: Optional[Block] = self.head
        while current_block is not None:
            current_hash = current_block.hash()
            if int(current_hash, 16) > self.hash_target_size:
                print("Corrupt block detected (invalid hash):\n", current_block)
                return False
            if current_block.previous_hash != previous_hash:
                print("Corrupt block detected (previous_hash mismatch):\n", current_block)
                return False
            previous_hash = current_hash
            current_block = current_block.next_block
        return True

    def show_blocks(self):
        """ Print all blocks"""
        current_block: Optional[Block] = self.head
        while current_block is not None:
            print(current_block)
            current_block = current_block.next_block
