"""Block"""
from datetime import datetime
from hashlib import sha256
from typing import Any


class Block:
    """Block"""

    def __init__(self, data):
        self.block_index = 0
        self.next_block: Any = None
        self.nonce = 0
        self.previous_hash = 0x0
        self.data = data
        self.timestamp = datetime.now()

    def hash(self):
        """Compute block hash"""

        content = (
            str(self.nonce)
            + str(self.data)
            + str(self.previous_hash)
            + str(self.timestamp)
            + str(self.block_index)
        )
        # returns a hexademical string
        return sha256(content.encode("utf-8")).hexdigest()

    def __str__(self):
        return (
            "Block: "
            + str(self.block_index)
            + "\nHash: "
            + str(self.hash())
            + "\nPrevious-Hash: "
            + str(self.previous_hash)
            + "\nData: "
            + str(self.data)
            + "\nNonce: "
            + str(self.nonce)
            + "\nTimestamp: "
            + str(self.timestamp)
            + "\n--------------"
        )
