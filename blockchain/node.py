"""A simple node"""
from blockchain.blockchain import Blockchain
from blockchain.block import Block


def main():
    """Test blockchain verification"""
    # initialize blockchain
    block_chain = Blockchain()

    # mine blocks
    for i in range(4):
        block_chain.mine(Block("data" + str(i + 1)))

    block_chain.show_blocks()

    print(block_chain.verify())

    # Modify third block
    block_chain.head.data = "Dirty data"

    print(block_chain.verify())


if __name__ == "__main__":
    main()
